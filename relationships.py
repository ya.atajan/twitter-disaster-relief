# Get relationships for a twitter use and returns users that follow each other in the same geographical location
def getRelationships(followers, following):
    return set(followers).intersection(following)