# Import regular expressions and read js file
import re
#read the js file
with open("Website\\coolScripts.js", 'r') as file:
    #store information in coolScripts in the filedata variable
    filedata = file.read()

# Replace the target string
filedata = re.sub("placeholder2345()", "myUpdatedMap()", filedata)

# Write changes to js file
with open("Website\\coolScripts.js", 'w') as file:
  file.write(filedata)