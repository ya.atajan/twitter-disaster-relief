import tweepy, datetime, time, relationships, random, googlemaps

consumer_key = "h51mCA7N0BzYDOG7uBg6eIEHE"
consumer_secret = "msiUSVC7UHjKozvYuHKuKuhLJO7gKEIJcvi5MickUxx4Qa9zr4"
access_token = "854494782248411137-KUqHkukVtiCja0SaBWHKjcNaWOhDtcM"
access_token_secret = "2kZeATXNqMuKtBAV4cFmMa2bCYslaPowaxz4ccupjpvnO"

auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_token_secret)

api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, )

followers = api.friends_ids()
following = api.followers_ids()
mutual = relationships.getRelationships(followers, following)

safe = []
not_safe = []
gmaps = googlemaps.Client(key="AIzaSyBw0cZiRfqmF4oNGsc_yjdqz7qL906O96Q")


def get_tweets(api, id):
    page = 1
    counter = 0
    name = api.get_user(id)
    deadend = False
    tweets = api.user_timeline(id=id, page=page)

    for tweet in tweets:
        if (datetime.datetime.now() - tweet.created_at).days < 1:
            if (counter < 1):
                update_status_positive(name)
            counter = + 1
            print(tweet.text.encode("utf-8"))
        else:
            update_status_negative(name)
            deadend = True
            return


# Updates status from the read file
def update_status_positive(screen_name, screen_name2):
    key = (random.randint(50000, 9993462))
    api.update_status(
        "Hey there @{}! If you know where @{} is, please @ us with some information! Message ID:{}".format(screen_name,
                                                                                                     screen_name2,
                                                                                                     str(key)))


# Updates status from the read file
def update_status_negative(screen_name):
    key = (random.randint(50000, 99997546))
    api.update_status(
        "Hey there @{}! Are you safe? Please @ us with 'safe' if you are! Message ID:{}".format(screen_name, str(key)))


# Get's the status id of a user.
def get_status_id(id):
    for status in api.user_timeline(id=id):
        return status.id


# def direct_messages():
#     api.send_direct_message(user = "2276755368", text = "This is being sent via python")


# Returns user's status.
def give_status_text(id):
    tweet = api.get_status(get_status_id(id))
    #print("-------------------")
    #print(tweet.text)
    #print("-------------------")
    return (tweet.text)


def head_count(id):
    page = 1
    counter = 0
    deadend = False
    tweets = api.user_timeline(id=id, page=page)

    for tweet in tweets:
        if (datetime.datetime.now() - tweet.created_at).days < 1:
            if (counter < 1):
                safe.append(id)
            counter = + 1
            print(tweet.text.encode("utf-8"))
        else:
            not_safe.append(id)
            deadend = True
            return


# See's if user is safe or not
def safe_or_not(id):
    tweets = {}
    s = give_status_text(id)
    l = ['ok', 'fine', 'great', 'alive', 'safe', 'i am safe']
    if any(word in s.lower() for word in l):
        safe.append(id)
        #print("Safe------------------------")
    else:
        not_safe.append(id)
        #print("Unknown---------------------")


def initial_run():
    for i in mutual:
        safe_or_not(i)

    for i in safe:
        tweet = api.get_status(get_status_id(i))
        print("SAFE: ")
        print(tweet.text)
        print(i)
        print(datetime.datetime.now())
        name = api.get_user(i)
        for k in not_safe:
            name2 = api.get_user(k)
            update_status_positive(name.screen_name, name2.screen_name)

    for i in not_safe:
        tweet = api.get_status(get_status_id(i))
        print("NOT SAFE: ")
        print(tweet.text)
        print(i)
        name = api.get_user(i)
        update_status_negative(name.screen_name)

# def run():
#     for i in mutual:
#         safe_or_not(i)
#
#     for i in safe:
#         print("SAFE: ")
#         print(i)
#         name = api.get_user(i)
#         for k in not_safe:
#             name2 = api.get_user(k)
#             # update_status_positive(name.screen_name, name2.screen_name)
#
#     for i in not_safe:
#         print("NOT SAFE: ")
#         print(i)
#         name = api.get_user(i)
#         # update_status_negative(name.screen_name)


## Show menu ##

def main():
    print(30 * '-')
    print("   M A I N - M E N U")
    print(30 * '-')
    print("1. Send Alert")
    #print("2. Update Map")
    print("2. Quit")
    print(30 * '-')

    ## Get input ###
    choice = input('Enter your choice [1-2] : ')

    ### Convert string to int type ##
    choice = int(choice)

    ### Take action as per selected menu-option ###
    if choice == 1:
        print("Gathering...")
        time.sleep(0.5)
        print("Sending push notifications...")
        initial_run()
        main()
    elif choice == 2:
        print("Good bye.")
    else:  ## default ##
        print("Invalid number. Try again...")




main()